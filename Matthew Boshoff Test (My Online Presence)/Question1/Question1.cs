﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Question1
{
    /*
        QUESTION 1
       
        Devise a function that takes an input 'n' (integer) and returns a string that is the
        decimal representation of that number grouped by commas after every 3 digits. You can't
        solve the task using a built-in formatting function that can accomplish the whole
        task on its own.

        Assume: 0 <= n < 1000000000

        1 -> "1"
        10 -> "10"
        100 -> "100"
        1000 -> "1,000"
        10000 -> "10,000"
        100000 -> "100,000"
        1000000 -> "1,000,000"
        35235235 -> "35,235,235"
     
     */
    class Question1
    {
        static void Main(string[] args)
        {
            var inputs = new List<int> { 1, 10, 100, 1000, 10000, 100000, 1000000, 35235235 };

            Console.WriteLine("Welcome to Question 1");
            Console.WriteLine();

            foreach (var input in inputs)
            {
                var formatted = formatInteger(input);

                /* format the input here */

                Console.WriteLine(input + " -> \"" + formatted + "\"");
            }

            Console.ReadLine();
        }

        private static String formatInteger(int value)
        {
            // Initialisation.
            char[] seperated = value.ToString().ToCharArray();
            StringBuilder formatted = new StringBuilder();
            int counter = 0;

            // Reverse the array for easier processing.
            Array.Reverse(seperated);

            // Loop through each character.
            foreach (char c in seperated)
            {
                // Once we reach the third instance add a comma.
                if (counter % 3 == 0 && counter != 0)
                {
                    formatted.Append(",");
                    counter = 0;
                }

                formatted.Append(c);
                counter++;
            }

            // Reverse the array again so it's in the correct order.
            seperated = formatted.ToString().ToCharArray();
            Array.Reverse(seperated);

            return new string(seperated);
        }
    }
}
