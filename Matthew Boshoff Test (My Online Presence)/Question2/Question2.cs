﻿using System;
using System.Collections.Generic;

namespace Question2
{
    /*
      Given a zero-based integer array of length N, the equivalence index (i) is the index where the sum of all the items to the left of the index
      are equal to the sum of all the items to the right of the index. Please CONSIDER performance.

      Constraints: 0 <= N <= 100 000

      Example: Given the following array [1, 2, 3, 4, 5, 7, 8, 10, 12]
      Your program should output "6" because 1 + 2 + 3 + 4 + 5 + 7 = 10 + 12

      if no index exists then output -1
     */
    class Question2
    {
        static void Main(string[] args)
        {
            var count = 1;
            var inputs = new List<List<int>>
            {
                    new List<int> { 1, 2, 3, 4, 5, 7, 8, 10, 12 },
                    new List<int> { 1, 4, 5, 6, 8, 10, 17},
                    new List<int> { 12, 56, 5, 6, 7, 9}
            };

            Console.WriteLine("Welcome to Question 2");
            Console.WriteLine();

            foreach (var input in inputs)
            {
                var equivalenceIndex = -1;
                int left = 0;
                int right = 0;

                // Add all the values into the right.
                foreach(int i in input)
                {
                    right += i;
                }

                for(int i = 0; i < input.Count; i++)
                {
                    // Check if the left are right are equal, excluding the current number we are on.
                    if (left == (right - input[i]))
                    {
                        equivalenceIndex = i;
                        break;
                    }
                        
                    // Alter the values.
                    left += input[i];
                    right -= input[i];
                }

                Console.Write("Input " + count + ": ");
                Console.WriteLine("[" + String.Join(", ", input) + "]");
                Console.Write("Equivalence index: " + equivalenceIndex);
                Console.WriteLine();
                Console.WriteLine();
                count++;
            }

            Console.ReadLine();
        }
    }
}
