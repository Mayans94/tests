﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Question3
{
    /*        
       QUESTION 3
       
       Design a Deck and Card class. Write a function to instantiate a deck of cards in a RANDOM order, loop through all cards and print
       out the card number + suit.
       
       Then shuffle and loop through and print out the card number + suit again.

     */
    class Question3
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to Question 3");
            Console.WriteLine("Initiating deck...");

            var deck = new Deck();

            foreach (var card in deck)
            {
                Console.WriteLine(card);
            }

            Console.WriteLine();
            Console.WriteLine("Shuffling deck...");
            Console.WriteLine();

            deck.Shuffle();

            foreach (var card in deck)
            {
                Console.WriteLine(card);
            }

            Console.WriteLine();
            Console.ReadLine();
        }
    }

    public class Deck : IEnumerable<Card>
    {
        private List<Card> _cards { get; set; } = new List<Card>();

        public IEnumerator<Card> GetEnumerator() => _cards.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public Deck()
        {
            String[] suits = new String [] {"HEARTS", "DIAMONDS", "SPADES", "CLUBS"};
            for(int i = 0; i < 4; i++)
            {
                for(int j = 0; j < 13; j++)
                {
                    _cards.Add(new Card(j + 1, suits[i]));
                }
            }

            Shuffle();
        }

        public void Shuffle()
        {
            Random range = new Random();
            int randomRange;
            int count = 52;
            Object holder;

            while(count > 1)
            {
                randomRange = range.Next(count);
                count--;
                holder = _cards[randomRange];
                _cards[randomRange] = _cards[count];
                _cards[count] = (Card)holder;
            }

            // Cut the Deck ;)
            List<Card> _cardsFirstHalf = new List<Card>();
            List<Card> _cardsSecondHalf = new List<Card>();

            for (int i = 0; i < 26; i ++)
            {
                _cardsFirstHalf.Add(_cards[i]);
            }

            for (int i = 26; i < 52; i++)
            {
                _cardsSecondHalf.Add(_cards[i]);
            }

            _cards.Clear();
            _cards.AddRange(_cardsSecondHalf);
            _cards.InsertRange(26, _cardsFirstHalf);
        }
    }

    public class Card
    {
        private int value;
        private String suit;

        public Card(int _value, String _suit)
        {
            value = _value;
            suit = _suit;
        }

        public override string ToString()
        {
            return value + " of " + suit;
        }
    }
}
