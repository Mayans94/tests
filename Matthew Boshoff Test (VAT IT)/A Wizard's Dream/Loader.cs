﻿using A_Wizard_s_Dream.Models;
using A_Wizard_s_Dream.Models.Creatures;
using A_Wizard_s_Dream.Models.Traits;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A_Wizard_s_Dream
{
    class Loader
    {
        static void Main(string[] args)
        {
            Board board = new Board();

            Console.WriteLine("---Loading Board---\n");
            LoadBoard(ref board);

            Console.WriteLine("\n\nChallenge One: \n");
            Console.WriteLine("---The Void Tile Features---\n");
            ChallengeOne(board,1);

            Console.WriteLine("\n\nChallenge Two: \n");
            ChallengeTwo(board);

            Console.ReadLine();
        }

        /// <summary>
        /// Used to load the board with data.
        /// </summary>
        /// <param name="board"></param>
        public static void LoadBoard(ref Board board)
        {
            // Load the Void Tile with data.
            Tile voidTile = new Tile(1, true);

            // Add Traits.
            voidTile.addTrait(new Darkness(),1);
            voidTile.addTrait(new Music(),2);
            voidTile.addTrait(new Music(),3);
            voidTile.addTrait(new Water(),4);
            voidTile.addTrait(new Water(),5);

            // Test Exception Trait.
            try
            {
                voidTile.addTrait(new Trait("TEST"), 6);
            }
            catch(Exception ex)
            {
                Console.WriteLine("TEST:\nException caught on Trait. Exception: " + ex.Message);
            }


            // Add Creatures.
            voidTile.addCreature(new Sponge(),0);
            voidTile.addCreature(new Sponge(), 1);
            voidTile.addCreature(new SpaceDude(),2);
            voidTile.addCreature(new SpaceDude(), 3);
            voidTile.addCreature(new SpaceDude(), 4);

            // Test Exception Creature.
            try
            {
                voidTile.addCreature(new Creature("TEST"), 0);
            }
            catch (Exception ex)
            {
                Console.WriteLine("TEST:\nException caught on Creature. Exception: " + ex.Message);
            }

            board.addVoidTile(voidTile);

            // Add Second Tile.
            Tile secondTile = new Tile(2, false);

            // Add Traits.
            secondTile.addTrait(new Water(), 0);
            secondTile.addTrait(new Water(), 1);
            secondTile.addTrait(new Water(), 2);
            secondTile.addTrait(new Water(), 3);
            secondTile.addTrait(new Water(), 4);

            // Add Creatures.
            secondTile.addCreature(new GreenEgg(), 0);
            secondTile.addCreature(new Virus(), 1);

            board.addTile(secondTile, 0);

            // Add Third Tile.
            Tile thirdTile = new Tile(3, false);

            // Add Traits.
            thirdTile.addTrait(new Air(), 3);
            thirdTile.addTrait(new Air(), 4);

            // Add Creatures.
            thirdTile.addCreature(new SpaceDude(), 3);

            board.addTile(thirdTile, 1);

            // Add Fourth Tile.
            Tile fourthTile = new Tile(4, false);

            // Add Traits.
            fourthTile.addTrait(new Stone(), 0);
            fourthTile.addTrait(new Stone(), 1);
            fourthTile.addTrait(new Stone(), 2);
            fourthTile.addTrait(new Stone(), 3);
            fourthTile.addTrait(new Stone(), 4);
            fourthTile.addTrait(new Stone(), 5);

            // Add Creatures.
            fourthTile.addCreature(new Virus(), 1);

            board.addTile(fourthTile, 2);

            // Add Fifth Tile.
            Tile fifthTile = new Tile(5, false);

            // Add Traits.
            fifthTile.addTrait(new Crystal(), 0);
            fifthTile.addTrait(new Darkness(), 1);
            fifthTile.addTrait(new Darkness(), 2);

            // Add Creatures.
            fifthTile.addCreature(new Virus(), 0);
            fifthTile.addCreature(new Virus(), 1);

            board.addTile(fifthTile, 4);

            Console.WriteLine("\n---Load Complete---");
        }

        /// <summary>
        /// Challenge One but will also be used for Part Two of Challenge Two
        /// </summary>
        /// <param name="board"></param>
        /// <param name="tile"></param>
        public static void ChallengeOne(Board board, int tile)
        {
            List<Tile> tiles = board.getTiles();

            // Cycle through the creatures and disply them if they are there.
            Console.WriteLine("Creatures:");
            Creature[] creatures = tiles[tile - 1].getCreatures();
            for (int i = 0; i < creatures.Count(); i++)
            {
                if (creatures[i] != null)
                {
                    Console.WriteLine(creatures[i].getName() + " at postition " + (i +1) + " with the trait on the left: " + creatures[i].getLeftTrait() + " and the trait on the right: " + creatures[i].getRightTrait());
                }
            }

            //Cycle through the traits and display them if they are there.
            Console.WriteLine("\nTraits:");
            Trait[] traits = tiles[tile - 1].getTraits();
            for (int i = 0; i < traits.Count(); i++)
            {
                if(traits[i] != null)
                { 
                    Console.WriteLine("Postion " + (i + 1) + ":" + traits[i].getType());
                }
            }

            // Build the stack first and count.
            Dictionary<String, int> stack = new Dictionary<string, int>();
            int possibleValue;
            Console.WriteLine("\nTrait Stack Count:");
            foreach (Trait t in traits)
            {
                if (t != null)
                { 
                    if (stack.TryGetValue(t.getType(), out possibleValue))
                    {
                        stack[t.getType()] = possibleValue + 1;
                    }
                    else
                    {
                        stack.Add(t.getType(), 1);
                    }
                }
            }

            // Display the full count of all the traits.
            foreach (KeyValuePair<String,int> s in stack)
            {
                Console.WriteLine(s.Key + ": " + s.Value);
            }

        }

        /// <summary>
        /// Challenge Two
        /// </summary>
        /// <param name="board"></param>
        public static void ChallengeTwo(Board board)
        {
            List<Tile> tiles = board.getTiles();

            // Tiles and Creatures surrounding the void Tile.
            Console.WriteLine("---PART ONE---");

            Console.WriteLine("\nTiles and Creatures surrounding the Void Tile:\n");
            // Cycle through the tiles and determine if there are links then display the info.
            for(int i = 0; i < 6; i++)
            {
                Console.WriteLine("On side " + (i+1) + " of the Void Tile is Tile: " + 
                    (tiles[0].getTileLink(i).Equals("NONE") ? tiles[0].getTileLink(i) : 
                    tiles[0].getTileLink(i) + getFacingCreatures(tiles, Convert.ToInt32((tiles[0].getTileLink(i))), i)));
            }

            // Placement for each tile.
            Console.WriteLine("\n\n---PART TWO---");

            foreach (Tile t in tiles)
            {
                // Exclude the void tile
                if (!t.isVoidTile())
                {
                    Console.WriteLine("\nPlacement For Tile " + t.getNumber() + ":");
                    ChallengeOne(board, t.getNumber());
                }
            }
        }

        /// <summary>
        /// Used to get the ceratures facing a certain side of the Void tile.
        /// </summary>
        /// <param name="_tiles"></param>
        /// <param name="facingTileNumber"></param>
        /// <param name="voidSide"></param>
        /// <returns></returns>
        private static String getFacingCreatures(List<Tile> _tiles, int facingTileNumber, int voidSide)
        {
            String output = " and the creatures facing this side: ";
            int[] opposites = new int[] { 3, 4, 5, 0, 1, 2 };
            List<Tile> tiles = _tiles;

            // Get the creatures for the tile facing the void tile side.
            Creature[] creatures = tiles[facingTileNumber - 1].getCreatures();

            // Determine if there are any creatures on the correct side facing the void tile.
            output += (creatures[opposites[voidSide]] == null ? "NONE and " : creatures[opposites[voidSide]].getName()  + " and ");

            // If it is the 0 side, we need to minus one to get the other creature, thus using 5.
            if (opposites[voidSide] == 0)
            {
                output += creatures[5] == null ? "NONE" : creatures[5].getName();
            }
            else
            {
                output += creatures[opposites[voidSide] - 1] == null ? "NONE" : creatures[opposites[voidSide] - 1].getName();
            }

            return output;
        }
    }
}
