﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A_Wizard_s_Dream.Models
{
    class Board
    {
        List<Tile> tiles = new List<Tile>();

        // Used to determine the second link of joining tiles.
        int[] opposites = new int[] { 3, 4, 5, 0, 1, 2 };

        /// <summary>
        /// Constructor.
        /// </summary>
        public Board()
        {
        }

        public void addVoidTile(Tile tile)
        {
            tiles.Add(tile);
        }

        /// <summary>
        /// Adds a Tile to the specified unique tile and on which side.
        /// </summary>
        /// <param name="uniqueTile"> The Tile for which the new Tile must be linked to.</param>
        /// <param name="side"> The side for which the new Tile must sit.</param>
        public void addTile(Tile tile, int side)
        {
            // Make the first link this tile to the void Tile.
            tile.linkTile(1, opposites[side]);

            // Update the void Tile to add the new link of this tile.
            tiles[0].linkTile(tile.getNumber(), side);

            tiles.Add(tile);
        }

        /// <summary>
        /// Gets the list of Tiles on the Board.
        /// </summary>
        /// <returns> The list of Tiles.</returns>
        public List<Tile> getTiles()
        {
            return tiles;
        }

    }
}
