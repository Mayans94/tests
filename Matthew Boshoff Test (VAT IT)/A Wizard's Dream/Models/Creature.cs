﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A_Wizard_s_Dream.Models
{
    class Creature
    {
        private String name;
        String[] traits = new String[2];

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="_name"></param>
        public Creature(String _name)
        {
            name = _name;
        }

        /// <summary>
        /// Gets the name of the Creature.
        /// </summary>
        /// <returns></returns>
        public String getName()
        {
            return name;
        }

        /// <summary>
        /// Adds the Trait to the Creature.
        /// </summary>
        /// <param name="traitName"></param>
        /// <param name="postion"></param>
        public void addTrait(String traitName, int postion)
        {
            traits[postion] = traitName;
        }

        /// <summary>
        /// Gets the Trait to the left of the Creature.
        /// </summary>
        /// <returns></returns>
        public String getLeftTrait()
        {
            return traits[0];
        }

        /// <summary>
        /// Gets the Trait to the Right of the Creature.
        /// </summary>
        /// <returns></returns>
        public String getRightTrait()
        {
            return traits[1];
        }
    }
}
