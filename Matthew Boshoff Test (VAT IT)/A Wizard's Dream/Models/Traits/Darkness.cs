﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A_Wizard_s_Dream.Models.Traits
{
    class Darkness : Trait
    {
        public Darkness() : base("DARKNESS")
        {
        }
    }
}
