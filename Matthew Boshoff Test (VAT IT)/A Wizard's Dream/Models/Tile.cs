﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A_Wizard_s_Dream.Models
{
    class Tile
    {
        int number;
        bool isVoid;
        int? [] sides = new int?[6];
        Trait [] traits = new Trait[6];
        Creature[] creatures = new Creature[6];

        /// <summary>
        /// Constructor.
        /// </summary>
        public Tile(int _number, bool _isVoid)
        {
            number = _number;
            isVoid = _isVoid;
        }

        /// <summary>
        /// Adds a Trait to the Tile.
        /// </summary>
        /// <param name="_trait"> The Trait Object to be added</param>
        /// <param name="position"> The position on the tile that the creature should be added.</param>
        public void addTrait(Trait _trait, int position)
        {
            if(position > 5 || traits[position] != null)
            { 
                throw new Exception("Trait postion out of range or already used.");
            }

            // Add the left trait if the Creature exists.
            if (creatures[position] != null)
            {
                creatures[position].addTrait( _trait.getType(), 0);
            }

            // Add the right trait if the Creature exists.
            if (position == 5)
            {
                if (creatures[0] != null)
                {
                    creatures[0].addTrait(_trait.getType(), 0);
                }
            }
            else
            {
                if (creatures[position + 1] != null)
                {
                    creatures[position + 1].addTrait(_trait.getType(), 0);
                }
            }

            traits[position] = _trait;
        }

        /// <summary>
        /// Adds a Creature to the Tile.
        /// </summary>
        /// <param name="_creature"> The Creature Object to be added.</param>
        /// <param name="position"> The position on the tile that the creature should be added.</param>
        public void addCreature(Creature _creature, int position)
        {
            if (position > 5 || creatures[position] != null)
            {
                throw new Exception("Creature postion out of range or already used.");
            }

            // Add the left trait
            _creature.addTrait((traits[position] == null ? "NONE" : traits[position].getType()), 0);

            // Add the right trait
            if (position == 5)
            {
                _creature.addTrait((traits[0] == null ? "NONE" : traits[0].getType()), 1);
            }
            else
            {
                _creature.addTrait((traits[position + 1] == null ? "NONE" : traits[position + 1].getType()), 1);
            }

            creatures[position] = _creature;
        }

        /// <summary>
        /// Determines if the Tile is the void Tile.
        /// </summary>
        /// <returns> If the Tile is the void Tile.</returns>
        public virtual Boolean isVoidTile()
        {
            return isVoid;
        }

        /// <summary>
        /// Determine Traits on the Tile.
        /// </summary>
        /// <returns> An array of the Traits on the tile.</returns>
        public Trait[] getTraits()
        {
            return traits;
        }

        /// <summary>
        /// Determine Creatures on the Tile.
        /// </summary>
        /// <returns> An array of the Creatures on the Tile.</returns>
        public Creature[] getCreatures()
        {
            return creatures;
        }

        /// <summary>
        /// Gets the unique number associated with the tile.
        /// </summary>
        /// <returns> The unique number for this tile.</returns>
        public int getNumber()
        {
            return number;
        }

        /// <summary>
        /// Links tiles based on their unique number and which side.
        /// </summary>
        /// <param name="uniqueNumber"></param>
        /// <param name="side"></param>
        public void linkTile(int uniqueNumber, int side)
        {
            if (sides[side] != null)
            {
                throw new Exception("Tile already has a link on that side. UniqueNumber: " + uniqueNumber + ", Side: " + side);
            }

            sides[side] = uniqueNumber;
        }

        /// <summary>
        /// Gets the link of a Tile based on the side.
        /// </summary>
        /// <param name="side"></param>
        /// <returns> The unique number of the tile or empty if there is no link on that side.</returns>
        public String getTileLink(int side)
        {
            if(sides[side] == null)
            {
                return "NONE";
            }
            else
            {
                return sides[side].ToString();
            }
        }
    }
}
