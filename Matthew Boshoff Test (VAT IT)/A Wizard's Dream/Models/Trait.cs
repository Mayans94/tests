﻿using System;
using System.Collections.Generic;

namespace A_Wizard_s_Dream.Models
{
    class Trait 
    {
        private String name;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="type"></param>
        public Trait(String type)
        {
            name = type;
        }

        /// <summary>
        /// Gets the name of the Type.
        /// </summary>
        /// <returns></returns>
        public String getType()
        {
            return name;
        }
    }
}
